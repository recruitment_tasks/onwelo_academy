package com.example.emailsender.util;

import com.example.emailsender.dto.UserEmailDto;
import com.example.emailsender.entity.EmailDetails;
import com.example.emailsender.entity.UserEmail;
import org.springframework.mail.SimpleMailMessage;

import java.util.HashSet;
import java.util.List;

public class InitData {

    public UserEmail createUserEmailOne() {
        UserEmail dto = new UserEmail();
        dto.setId(1L);
        dto.setEmail("user1@email.com");
        return dto;
    }

    public UserEmail createUserEmailTwo() {
        UserEmail dto = new UserEmail();
        dto.setId(2L);
        dto.setEmail("user2@email.com");
        return dto;
    }

    public UserEmailDto createUserEmailDto() {
        UserEmailDto dto = new UserEmailDto();
        dto.setId(5L);
        dto.setEmail("user.dto5@email.com");
        return dto;
    }

    public UserEmailDto createUserEmailDtoWithInvalidEmail() {
        UserEmailDto dto = new UserEmailDto();
        dto.setId(20L);
        dto.setEmail("user.dto20");
        return dto;
    }

    public EmailDetails createMailOne() {
        EmailDetails mail = new EmailDetails();
        mail.setId(1L);
        mail.setSubject("Test subject");
        mail.setMsgBody("Test message body");
        return mail;
    }

    public EmailDetails createMailWithRecipients(List<UserEmail> recipients) {
        EmailDetails mail = new EmailDetails();
        mail.setId(1L);
        mail.setSubject("Test subject");
        mail.setMsgBody("Test message body");
        mail.setRecipients(new HashSet<>(recipients));
        return mail;
    }

    public EmailDetails createMailWithoutSubject() {
        EmailDetails mail = new EmailDetails();
        mail.setId(1L);
        mail.setMsgBody("Test message body");
        return mail;
    }
    public EmailDetails createMailWithoutMsgBody() {
        EmailDetails mail = new EmailDetails();
        mail.setId(1L);
        mail.setSubject("Test subject");
        return mail;
    }

    public SimpleMailMessage createSimpleMailMessage() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("from@email.com");
        simpleMailMessage.setTo("to@email.com");
        simpleMailMessage.setSubject("subject");
        simpleMailMessage.setText("text");
        return simpleMailMessage;
    }
}
