package com.example.emailsender.rest;

import com.example.emailsender.entity.EmailDetails;
import com.example.emailsender.entity.UserEmail;
import com.example.emailsender.service.EmailServiceImpl;
import com.example.emailsender.util.InitData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmailRestController.class)
class EmailRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmailServiceImpl service;

    @Autowired
    ObjectMapper objectMapper;

    InitData initData = new InitData();

    @Nested
    class SendMailToAllSubscribers {
        @Test
        void returns200_givenCorrectEmailDetails() throws Exception {
            //given
            UserEmail userEmailOne = initData.createUserEmailOne();
            UserEmail userEmailTwo = initData.createUserEmailTwo();
            EmailDetails expected = initData.createMailWithRecipients(
                    List.of(userEmailOne, userEmailTwo));
            String expectedSuccessMsg = "Mail sent successfully.";
            given(service.saveMailAndSendToSubscribers(expected)).willReturn(expectedSuccessMsg);
            //when
            MvcResult mvcResult = mockMvc.perform(post("/api/emails")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isOk())
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            then(service).should().saveMailAndSendToSubscribers(expected);
            assertThat(actualResponseBody).as("Check if response is equal to %s details", "Email")
                    .isEqualTo(expectedSuccessMsg);
        }

        @Test
        void returns400_givenNull() throws Exception {
            //given
            UserEmail userEmailOne = initData.createUserEmailOne();
            UserEmail userEmailTwo = initData.createUserEmailTwo();
            EmailDetails expected = initData.createMailWithRecipients(
                    List.of(userEmailOne, userEmailTwo));
            given(service.saveMailAndSendToSubscribers(expected)).willReturn(null);
            //when
            mockMvc.perform(post("/api/emails")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isBadRequest())
                    .andDo(print());
            //then
            then(service).should().saveMailAndSendToSubscribers(expected);
        }

        @Test
        void returns400_witErrorMsg_givenEmailDetailsWithoutSubject() throws Exception {
            //given
            String expectedHttpStatusCodeAsString = String.valueOf(HttpStatus.BAD_REQUEST.value());
            String expectedErrorMsg = "Email subject must not be empty";
            EmailDetails expected = initData.createMailWithoutSubject();
            //when
            MvcResult mvcResult = mockMvc.perform(post("/api/emails")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isBadRequest())
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            assertThat(actualResponseBody).as("Check error message")
                    .contains(expectedHttpStatusCodeAsString, expectedErrorMsg);
        }

        @Test
        void returns400_witErrorMsg_givenEmailDetailsWithoutMsgBody() throws Exception {
            //given
            String expectedHttpStatusCodeAsString = String.valueOf(HttpStatus.BAD_REQUEST.value());
            String expectedErrorMsg = "Email message must not be empty";
            EmailDetails expected = initData.createMailWithoutMsgBody();
            //when
            MvcResult mvcResult = mockMvc.perform(post("/api/emails")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isBadRequest())
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            assertThat(actualResponseBody).as("Check error message")
                    .contains(expectedHttpStatusCodeAsString, expectedErrorMsg);
        }
    }
}