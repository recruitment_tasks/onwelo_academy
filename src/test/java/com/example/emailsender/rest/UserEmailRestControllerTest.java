package com.example.emailsender.rest;

import com.example.emailsender.dto.UserEmailDto;
import com.example.emailsender.service.UserEmailServiceImpl;
import com.example.emailsender.util.InitData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserEmailRestController.class)
class UserEmailRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserEmailServiceImpl service;

    @Autowired
    ObjectMapper objectMapper;

    InitData initData = new InitData();

    @Nested
    class GetAllUserEmails {
        @Test
        void returns200_whenUserEmailsInDatabase() throws Exception {
            //given
            UserEmailDto expected1 = initData.createUserEmailDto();
            UserEmailDto expected2 = initData.createUserEmailDto();
            List<UserEmailDto> userEmails = List.of(expected1, expected2);
            given(service.fetchAll()).willReturn(userEmails);
            //when
            MvcResult mvcResult = mockMvc.perform(get("/api/users")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.size()").value(userEmails.size()))
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            then(service).should().fetchAll();
            assertThat(actualResponseBody).as("Check if response body contains %s details", "user email 1")
                    .contains(expected1.getId().toString(), expected1.getEmail());
            assertThat(actualResponseBody).as("Check if response body contains %s details", "user email 1")
                    .contains(expected2.getId().toString(), expected2.getEmail());
        }

        @Test
        void returns404_whenNoUserEmailsInDatabase() throws Exception {
            //given
            List<UserEmailDto> emptyList = List.of();
            given(service.fetchAll()).willReturn(emptyList);
            //when
            mockMvc.perform(get("/api/users")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNotFound())
                    .andDo(print());
            //then
            then(service).should().fetchAll();
        }
    }

    @Nested
    class GetUserEmail {
        @Test
        void returns200_givenCorrectId() throws Exception {
            //given
            UserEmailDto expected = initData.createUserEmailDto();
            given(service.fetchById(anyLong())).willReturn(expected);
            //when
            MvcResult mvcResult = mockMvc.perform(get("/api/users/{id}", expected.getId())
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            then(service).should().fetchById(anyLong());
            assertThat(actualResponseBody).as("Check if response is equal to %s details", "user email")
                    .isEqualTo(objectMapper.writeValueAsString(expected));
        }

        @Test
        void returns404_givenIncorrectId() throws Exception {
            //given
            Long id = 1L;
            given(service.fetchById(anyLong())).willReturn(null);
            //when
            mockMvc.perform(get("/api/users/{id}", id)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNotFound())
                    .andDo(print());
            //then
            then(service).should().fetchById(anyLong());
        }
    }

    @Nested
    class CreateUserEmail {
        @Test
        void returns200_givenCorrectDto() throws Exception {
            //given
            UserEmailDto expected = initData.createUserEmailDto();
            given(service.save(any(UserEmailDto.class))).willReturn(expected);
            //when
            MvcResult mvcResult = mockMvc.perform(post("/api/users")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isCreated())
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            then(service).should().save(any(UserEmailDto.class));
            assertThat(actualResponseBody).as("Check if response is equal to %s details", "user email")
                    .isEqualTo(objectMapper.writeValueAsString(expected));
        }

        @Test
        void returns400_givenNull() throws Exception {
            //given
            UserEmailDto expected = initData.createUserEmailDto();
            given(service.save(any(UserEmailDto.class))).willReturn(null);
            //when
            mockMvc.perform(post("/api/users")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isBadRequest())
                    .andDo(print());
            //then
            then(service).should().save(any(UserEmailDto.class));
        }

        @Test
        void returns400_withErrorMsg_givenInvalidEmailAddress() throws Exception {
            //given
            String expectedHttpStatusCodeAsString = String.valueOf(HttpStatus.BAD_REQUEST.value());
            String expectedErrorMsg = "Format of Email address is invalid.";
            UserEmailDto expected = initData.createUserEmailDtoWithInvalidEmail();
            //when
            MvcResult mvcResult = mockMvc.perform(post("/api/users")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isBadRequest())
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            assertThat(actualResponseBody).as("Check error message")
                    .contains(expectedHttpStatusCodeAsString, expectedErrorMsg);
        }
    }

    @Nested
    class UpdateUserEmail {
        @Test
        void returns200_givenCorrectDto() throws Exception {
            //given
            UserEmailDto expected = initData.createUserEmailDto();
            given(service.update(any(UserEmailDto.class))).willReturn(expected);
            //when
            MvcResult mvcResult = mockMvc.perform(put("/api/users")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isOk())
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            then(service).should().update(any(UserEmailDto.class));
            assertThat(actualResponseBody).as("Check if response is equal to %s details", "user email")
                    .isEqualTo(objectMapper.writeValueAsString(expected));
        }

        @Test
        void returns400_givenNull() throws Exception {
            //given
            UserEmailDto expected = initData.createUserEmailDto();
            given(service.update(any(UserEmailDto.class))).willReturn(null);
            //when
            mockMvc.perform(put("/api/users")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isBadRequest())
                    .andDo(print());
            //then
            then(service).should().update(any(UserEmailDto.class));
        }

        @Test
        void returns400_withErrorMsg_givenInvalidEmailAddress() throws Exception {
            //given
            String expectedHttpStatusCodeAsString = String.valueOf(HttpStatus.BAD_REQUEST.value());
            String expectedErrorMsg = "Format of Email address is invalid.";
            UserEmailDto expected = initData.createUserEmailDtoWithInvalidEmail();
            //when
            MvcResult mvcResult = mockMvc.perform(put("/api/users")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(expected)))
                    .andExpect(status().isBadRequest())
                    .andDo(print())
                    .andReturn();
            String actualResponseBody = mvcResult.getResponse().getContentAsString();
            //then
            assertThat(actualResponseBody).as("Check error message")
                    .contains(expectedHttpStatusCodeAsString, expectedErrorMsg);
        }
    }

    @Nested
    class DeleteUserEmail {
        @Test
        void returns202_givenCorrectId() throws Exception {
            //given
            UserEmailDto expected = initData.createUserEmailDto();
            //when
            mockMvc.perform(delete("/api/users/{id}", expected.getId()))
                    .andExpect(status().isAccepted())
                    .andDo(print());
            //then
            then(service).should().removeById(expected.getId());
        }
    }
}