package com.example.emailsender.service;

import com.example.emailsender.dto.UserEmailDto;
import com.example.emailsender.entity.UserEmail;
import com.example.emailsender.repository.UserEmailRepository;
import com.example.emailsender.util.InitData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
class UserEmailServiceImplTest {

    @Mock
    UserEmailRepository repository;

    @InjectMocks
    UserEmailServiceImpl service;

    @Spy
    ModelMapper mapper;

    @Spy
    InitData initData;

    @Captor
    ArgumentCaptor<UserEmail> userEmailArgumentCaptor;

    @Test
    void fetchAll_shouldReturnAllUserEmails() {
        //given
        UserEmail expectedUserEmail1 = initData.createUserEmailOne();
        UserEmail expectedUserEmail2 = initData.createUserEmailTwo();
        List<UserEmail> userEmails = List.of(expectedUserEmail1, expectedUserEmail2);
        given(repository.findAll()).willReturn(userEmails);
        //when
        List<UserEmailDto> actual = service.fetchAll();
        //then
        then(repository).should().findAll();
        then(repository).shouldHaveNoMoreInteractions();
        assertThat(actual).isNotNull().isNotEmpty().hasSize(2);
        UserEmailDto actualUserEmail1 = actual.get(0);
        UserEmailDto actualUserEmail2 = actual.get(1);
        assertAll("UserEmail1 properties",
                () -> assertThat(actualUserEmail1.getId()).as("Check %s's %s", "userEmail1", "id")
                        .isEqualTo(expectedUserEmail1.getId()),
                () -> assertThat(actualUserEmail1.getEmail()).as("Check %s's %s", "userEmail1", "email")
                        .isEqualTo(expectedUserEmail1.getEmail())
        );
        assertAll("UserEmail2 properties",
                () -> assertThat(actualUserEmail2.getId()).as("Check %s's %s", "UserEmail2", "id")
                        .isEqualTo(expectedUserEmail2.getId()),
                () -> assertThat(actualUserEmail2.getEmail()).as("Check %s's %s", "UserEmail2", "email")
                        .isEqualTo(expectedUserEmail2.getEmail())
        );
    }

    @Test
    void fetchById_shouldReturnUserEmailDto_givenCorrectId() {
        //given
        UserEmail expected = initData.createUserEmailOne();
        given(repository.findById(anyLong())).willReturn(Optional.of(expected));
        //when
        UserEmailDto actual = service.fetchById(expected.getId());
        //then
        ArgumentCaptor<Long> idArgumentCaptor = ArgumentCaptor.forClass(Long.class);
        then(repository).should().findById(idArgumentCaptor.capture());
        then(repository).shouldHaveNoMoreInteractions();
        Long actualId = idArgumentCaptor.getValue();

        assertThat(actual).as("Check if %s is not null", "UserEmail").isNotNull();
        assertAll("UserEmail properties",
                () -> assertThat(actualId).as("Check %s's %s", "userEmail1", "id")
                        .isEqualTo(expected.getId()),
                () -> assertThat(actual.getEmail()).as("Check %s's %s", "userEmail1", "email")
                        .isEqualTo(expected.getEmail())
        );
    }

    @Test
    void fetchById_throwsNoSuchElementException_givenWrongId() {
        //given
        Long id = 10L;
        //when
        Throwable thrown = catchThrowable(() -> service.fetchById(id));
        //then
        assertThat(thrown)
                .isExactlyInstanceOf(NoSuchElementException.class)
                .hasMessage("UserEmail with ID: " + id + " does not exist");
    }

    @Test
    void save_shouldSaveUserEmail_givenCorrectUserEmailDto() {
        //given
        UserEmailDto dto = initData.createUserEmailDto();
        UserEmail expected = mapper.map(dto, UserEmail.class);
        given(repository.save(any(UserEmail.class))).willReturn(expected);
        //when
        service.save(dto);
        //then
        then(repository).should().save(userEmailArgumentCaptor.capture());
        then(repository).shouldHaveNoMoreInteractions();
        UserEmail actual = userEmailArgumentCaptor.getValue();
        assertThat(actual).as("Check if %s is not null", "UserEmail").isNotNull();
        assertAll("UserEmail properties",
                () -> assertThat(actual.getId()).as("Check %s's %s", "userEmail1", "id")
                        .isEqualTo(expected.getId()),
                () -> assertThat(actual.getEmail()).as("Check %s's %s", "userEmail1", "email")
                        .isEqualTo(expected.getEmail())
        );
    }

    @Test
    void update_shouldUpdateUserEmail_givenCorrectUserEmailDto() {
        //given
        UserEmail entityBeforeUpdate = initData.createUserEmailTwo();
        UserEmail expected = new UserEmail();
        expected.setId(entityBeforeUpdate.getId());
        expected.setEmail("new.email@gmail.com");
        UserEmailDto dto = mapper.map(expected, UserEmailDto.class);
        given(repository.findById(anyLong())).willReturn(Optional.of(entityBeforeUpdate));
        given(repository.save(any(UserEmail.class))).willReturn(expected);
        //when
        service.update(dto);
        //then
        then(repository).should().findById(anyLong());
        then(repository).should().save(userEmailArgumentCaptor.capture());
        then(repository).shouldHaveNoMoreInteractions();
        UserEmail actual = userEmailArgumentCaptor.getValue();
        assertThat(actual).as("Check if %s is not null", "UserEmail").isNotNull();
        assertAll("UserEmail properties",
                () -> assertThat(actual.getId()).as("Check %s's %s", "userEmail1", "id")
                        .isEqualTo(expected.getId()),
                () -> assertThat(actual.getEmail()).as("Check %s's %s", "userEmail1", "email")
                        .isEqualTo(expected.getEmail())
        );
    }

    @Test
    void removeById_shouldDeleteUserEmail_givenCorrectId() {
        //given
        UserEmail expected = initData.createUserEmailOne();
        given(repository.findById(anyLong())).willReturn(Optional.of(expected));
        //when
        service.removeById(expected.getId());
        //then
        then(repository).should().findById(anyLong());
        then(repository).should().delete(any(UserEmail.class));
        then(repository).shouldHaveNoMoreInteractions();
    }
}