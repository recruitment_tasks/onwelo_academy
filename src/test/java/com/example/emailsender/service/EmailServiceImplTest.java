package com.example.emailsender.service;

import com.example.emailsender.dto.UserEmailDto;
import com.example.emailsender.entity.EmailDetails;
import com.example.emailsender.entity.UserEmail;
import com.example.emailsender.repository.EmailDetailsRepository;
import com.example.emailsender.util.InitData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class EmailServiceImplTest {

    @Mock
    EmailDetailsRepository repository;

    @Mock
    JavaMailSender javaMailSender;

    @Mock
    UserEmailServiceImpl userEmailService;

    @InjectMocks
    EmailServiceImpl service;

    @Spy
    ModelMapper mapper;

    @Spy
    InitData initData;

    @Captor
    ArgumentCaptor<EmailDetails> emailDetailsArgumentCaptor;

    @Test
    void saveMailAndSendToSubscribers_returnsSuccessMessage_givenCorrectInput() {
        //given
        String expectedResultMessage = "Mail sent successfully.";
        UserEmail userEmailOne = initData.createUserEmailOne();
        UserEmailDto userEmailDtoOne = mapper.map(userEmailOne, UserEmailDto.class);
        UserEmail userEmailTwo = initData.createUserEmailTwo();
        UserEmailDto userEmailDtoTwo = mapper.map(userEmailTwo, UserEmailDto.class);
        List<UserEmailDto> userEmailDtos = List.of(userEmailDtoOne, userEmailDtoTwo);
        EmailDetails expected = initData.createMailOne();

        given(userEmailService.fetchAll()).willReturn(userEmailDtos);
        given(repository.save(any(EmailDetails.class))).willReturn(expected);
        //when
        String actualResultMessage = service.saveMailAndSendToSubscribers(expected);
        //then
        then(userEmailService).should(times(2)).fetchAll();
        then(repository).should().save(emailDetailsArgumentCaptor.capture());
        then(javaMailSender).should().send(any(SimpleMailMessage.class));

        EmailDetails actual = emailDetailsArgumentCaptor.getValue();
        assertThat(actual).as("Check if %s is not null", "EmailDetails").isNotNull();
        assertThat(actualResultMessage).as("Check Result message").isEqualTo(expectedResultMessage);
        assertAll("EmailDetails properties",
                () -> assertThat(actual.getId()).as("Check %s's %s", "EmailDetails", "id")
                        .isEqualTo(expected.getId()),
                () -> assertThat(actual.getSubject()).as("Check %s's %s", "EmailDetails", "email")
                        .isEqualTo(expected.getSubject()),
                () -> assertThat(actual.getMsgBody()).as("Check %s's %s", "EmailDetails", "email")
                        .isEqualTo(expected.getMsgBody()),
                () -> assertThat(actual.getRecipients()).as("Check %s's %s", "EmailDetails", "email")
                        .contains(userEmailOne, userEmailTwo)
        );
    }

    @Test
    void sendMail_shouldGetInvoked_givenMessage() {
        //given
        SimpleMailMessage message = initData.createSimpleMailMessage();
        //when
        service.sendMail(message);
        //then
        then(javaMailSender).should().send(any(SimpleMailMessage.class));
    }

    @Test
    void sendMail_shouldGetInvokedNTimes() throws InterruptedException {
        //given
        int numberOfThreads = 5;
        SimpleMailMessage message = initData.createSimpleMailMessage();
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        //when
        for (int i = 0; i < numberOfThreads; i++) {
            executorService.submit(() -> {
                service.sendMail(message);
                latch.countDown();
            });
        }
        latch.await();
        //then
        then(javaMailSender).should(times(numberOfThreads)).send(any(SimpleMailMessage.class));
    }
}