package com.example.emailsender.rest;

import com.example.emailsender.entity.EmailDetails;
import com.example.emailsender.service.EmailServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/emails")
@RequiredArgsConstructor
public class EmailRestController {

    private final EmailServiceImpl emailService;

    @PostMapping
    public ResponseEntity<String> sendMailToAllSubscribers(@Valid @RequestBody EmailDetails details) {
        String status = emailService.saveMailAndSendToSubscribers(details);
        return status != null ?
                ResponseEntity.ok(status) :
                ResponseEntity.badRequest().build();
    }
}
