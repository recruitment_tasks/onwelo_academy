package com.example.emailsender.rest;

import com.example.emailsender.dto.UserEmailDto;
import com.example.emailsender.service.UserEmailServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
class UserEmailRestController {

    private final UserEmailServiceImpl service;

    @GetMapping
    ResponseEntity<List<UserEmailDto>> getAllUserEmails() {
        List<UserEmailDto> result = service.fetchAll();
        return result.isEmpty() ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(result);
    }

    @GetMapping("{id}")
    ResponseEntity<UserEmailDto> getUserEmail(@PathVariable Long id) {
        UserEmailDto dto = service.fetchById(id);
        return dto != null ?
                ResponseEntity.ok(dto) :
                ResponseEntity.notFound().build();
    }

    @PostMapping
    ResponseEntity<UserEmailDto> createUserEmail(@Valid @RequestBody UserEmailDto dto) {
        UserEmailDto created = service.save(dto);
        return created != null ?
                ResponseEntity.status(HttpStatus.CREATED)
                        .body(created) :
                ResponseEntity.badRequest().build();
    }

    @PutMapping
    ResponseEntity<UserEmailDto> updateUserEmail(@Valid @RequestBody UserEmailDto dto) {
        UserEmailDto updated = service.update(dto);
        return updated != null ?
                ResponseEntity.ok(updated) :
                ResponseEntity.badRequest().build();
    }

    @DeleteMapping("{id}")
    ResponseEntity<Void> deleteUserEmail(@PathVariable Long id) {
        service.removeById(id);
        return ResponseEntity.accepted().build();
    }
}
