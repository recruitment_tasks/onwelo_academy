package com.example.emailsender.service;

import com.example.emailsender.entity.EmailDetails;

public interface EmailService {

    String saveMailAndSendToSubscribers(EmailDetails details);
}
