package com.example.emailsender.service;

import com.example.emailsender.entity.EmailDetails;
import com.example.emailsender.entity.UserEmail;
import com.example.emailsender.repository.EmailDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
public class EmailServiceImpl implements EmailService {

    public static final String MAIL_SENT_SUCCESSFULLY_MSG = "Mail sent successfully.";
    private final EmailDetailsRepository repository;
    private final JavaMailSender javaMailSender;
    private final UserEmailServiceImpl userEmailService;
    private final ModelMapper mapper;

    @Value("${spring.mail.username}")
    private String sender;

    @Override
    @Transactional
    public String saveMailAndSendToSubscribers(EmailDetails details) {
        log.debug("Received request to send {}", details);
        SimpleMailMessage message = createMailMessage(details);
        sendMail(message);
        saveEmailToDatabase(details);
        return MAIL_SENT_SUCCESSFULLY_MSG;
    }

    @Async
    void sendMail(SimpleMailMessage message) {
        Thread thread = new Thread(() -> javaMailSender.send(message));
        thread.start();
        log.info(MAIL_SENT_SUCCESSFULLY_MSG);
        log.debug("{} sent successfully", message);
    }

    private void saveEmailToDatabase(EmailDetails details) {
        log.debug("Received request to save {}", details);
        getMailSubscribers().forEach(details::addRecipient);
        EmailDetails saved = repository.save(details);
        log.info("Mail with ID: {} was saved to database", saved.getId());
        log.debug("{} was saved successfully", saved);
    }

    private SimpleMailMessage createMailMessage(EmailDetails details) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(sender);
        message.setTo(getMailSubscribersEmails());
        message.setText(details.getMsgBody());
        message.setSubject(details.getSubject());
        return message;
    }

    private String[] getMailSubscribersEmails() {
        return getMailSubscribers().stream()
                .map(UserEmail::getEmail).toArray(String[]::new);
    }

    private Set<UserEmail> getMailSubscribers() {
        log.debug("Received request to get all mail subscribers");
        return userEmailService.fetchAll().stream()
                .map(userEmailDto -> mapper.map(userEmailDto, UserEmail.class))
                .collect(Collectors.toSet());
    }
}
