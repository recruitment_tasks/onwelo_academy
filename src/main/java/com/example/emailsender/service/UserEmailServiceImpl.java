package com.example.emailsender.service;

import com.example.emailsender.dto.UserEmailDto;
import com.example.emailsender.entity.UserEmail;
import com.example.emailsender.repository.UserEmailRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
@Log4j2
public class UserEmailServiceImpl implements UserEmailService {

    private final UserEmailRepository repository;
    private final ModelMapper mapper;

    @Override
    public List<UserEmailDto> fetchAll() {
        log.debug("Received request to find all UserEmails");
        return repository.findAll().stream().map(userEmail -> mapper.map(userEmail, UserEmailDto.class)).toList();
    }

    @Override
    public UserEmailDto fetchById(Long id) throws NoSuchElementException {
        log.debug("Received request to find UserEmail with ID: {}", id);
        UserEmailDto dto = repository.findById(id).map(userEmail -> mapper.map(userEmail, UserEmailDto.class))
                .orElseThrow(() -> new NoSuchElementException("UserEmail with ID: " + id + " does not exist"));
        log.info("Found UserEmail with ID: {}", id);
        log.debug("{} was found", dto);
        return dto;
    }

    @Override
    @Transactional
    public UserEmailDto save(UserEmailDto dto) {
        log.debug("Creating a UserEmail.");
        UserEmail saved = repository.save(mapper.map(dto, UserEmail.class));
        log.info("UserEmail with ID: {} was created successfully", saved.getId());
        log.debug("{} was created successfully", dto);
        return mapper.map(saved, UserEmailDto.class);
    }

    @Override
    @Transactional
    public UserEmailDto update(UserEmailDto dto) {
        UserEmailDto byId = fetchById(dto.getId());
        log.debug("Received request to update {}", byId);
        UserEmail updated = repository.save(mapper.map(dto, UserEmail.class));
        log.info("UserEmail with ID: {} was updated successfully", updated.getId());
        log.debug("{} was updated successfully", updated);
        return mapper.map(updated, UserEmailDto.class);
    }

    @Override
    @Transactional
    public void removeById(Long id) {
        UserEmailDto dto = fetchById(id);
        log.debug("Received request to delete {}", dto);
        repository.delete(mapper.map(dto, UserEmail.class));
        log.info("UserEmail with ID: {} was deleted successfully", id);
    }
}
