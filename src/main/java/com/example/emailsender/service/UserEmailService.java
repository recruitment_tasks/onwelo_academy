package com.example.emailsender.service;


import com.example.emailsender.dto.UserEmailDto;

import java.util.List;

public interface UserEmailService {

    List<UserEmailDto> fetchAll();

    UserEmailDto fetchById(Long id);

    UserEmailDto save(UserEmailDto dto);

    UserEmailDto update(UserEmailDto dto);

    void removeById(Long id);
}
