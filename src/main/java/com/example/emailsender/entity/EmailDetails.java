package com.example.emailsender.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
public class EmailDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Email subject must not be empty")
    private String subject;

    @NotEmpty(message = "Email message must not be empty")
    private String msgBody;

    @ManyToMany
    @JoinTable(name = "email_recipients",
            joinColumns = {@JoinColumn(name = "email_id")},
            inverseJoinColumns = {@JoinColumn(name = "recipient_id")})
    @ToString.Exclude
    private Set<UserEmail> recipients = new HashSet<>();

    public void addRecipient(UserEmail recipient) {
        if (recipients.contains(recipient)) {
            return;
        }
        recipients.add(recipient);
        recipient.addMailMessage(this);
    }

    public void removeRecipient(UserEmail recipient) {
        if (!recipients.contains(recipient)) {
            return;
        }
        recipients.remove(recipient);
        recipient.removeMailMessage(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmailDetails that = (EmailDetails) o;

        if (!id.equals(that.id)) return false;
        if (!Objects.equals(subject, that.subject)) return false;
        if (!Objects.equals(msgBody, that.msgBody)) return false;
        return Objects.equals(recipients, that.recipients);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + subject.hashCode();
        result = 31 * result + msgBody.hashCode();
        result = 31 * result + recipients.hashCode();
        return result;
    }
}
