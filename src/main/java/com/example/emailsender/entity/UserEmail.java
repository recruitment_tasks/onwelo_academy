package com.example.emailsender.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
public class UserEmail {

    @Id
    private Long id;

    private String email;

    @ManyToMany(mappedBy = "recipients")
    @ToString.Exclude
    private Set<EmailDetails> mailMessages = new HashSet<>();


    public void addMailMessage(EmailDetails message) {
        if (mailMessages.contains(message)) {
            return;
        }
        mailMessages.add(message);
        message.addRecipient(this);
    }

    public void removeMailMessage(EmailDetails message) {
        if (!mailMessages.contains(message)) {
            return;
        }
        mailMessages.remove(message);
        message.removeRecipient(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEmail userEmail = (UserEmail) o;

        if (!Objects.equals(id, userEmail.id)) return false;
        return Objects.equals(email, userEmail.email);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
