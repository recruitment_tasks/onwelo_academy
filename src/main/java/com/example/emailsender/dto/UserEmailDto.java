package com.example.emailsender.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
public class UserEmailDto {

    private Long id;

    @Email(message = "Format of Email address is invalid.")
    private String email;
}
