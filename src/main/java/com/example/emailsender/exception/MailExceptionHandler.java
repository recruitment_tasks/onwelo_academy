package com.example.emailsender.exception;


import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSendException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Log4j2
@ControllerAdvice
class MailExceptionHandler {

    public static final String NO_RECIPIENT_ADDRESSES_EXCEPTION_MSG = "No recipient addresses in database. Add recipient email addresses before sending mails";

    @ExceptionHandler(MailSendException.class)
    protected ResponseEntity<String> handleMailSendException() {
        log.warn("An exception occurred, which will cause a '{}' response", NO_RECIPIENT_ADDRESSES_EXCEPTION_MSG);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(NO_RECIPIENT_ADDRESSES_EXCEPTION_MSG);
    }
}
