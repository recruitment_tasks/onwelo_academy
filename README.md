# EmailSender


### Description
Service for sending emails to subscribers

Spring-Boot app that provides API for saving users (subscribers) to database and sending emails to every sub.
Input data (user's emails and mail messages) is validated. Custom Exception Handlers have been added.

App is logging details about incoming requests from API.

Service is tested with Unit tests using Junit5 and Mockito. Rest Controllers are tested with WebMvcTest.

### Service features:

* API for User and for Email Sending
* CRUD operations on user email
* Endpoint for sending email to all users in database
* Service is logging status of all incoming requests
* Each request for sending email starts new Thread

### Technologies used:

* Spring Boot 2.7.5
* Spring Data (JPA and REST)
* Swagger
* Hibernate
* Tests:
    * Junit5
    * Mockito
    * AssertJ
    * WebMvcTest
* In-memory database: H2

### How to run app?

1. Clone GitLab repository
2. Build project and perform tests
    * Open terminal in project directory
    * Type: `mvn clean install`

3. Start application
`mvn spring-boot:run`

4. Test app's API endpoints with [Swagger](http://localhost:8080/swagger-ui/index.html) or Postman

5. Press CTRL+C to finish running app